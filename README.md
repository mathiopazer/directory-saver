#  Directory Saver 

A small data retention script written in python, using **pathlib**, **zipfile** and **apscheduler**.

### Requirement 
Python's [**schedule**](https://github.com/dbader/schedule) need to be installed on your environment for this script to work properly.

Run to install : 
````bash
pip install schedule
````

### Usage
Put **dirsaver.py** in the retaining directory parent folder and run :

**Unix :**
```bash
python3 dirsaver.py
```
**Windows :**
```bash
python dirsaver.py
```
You will need to restart this script at each system reboot.

### Directory selection
By default, **dirsaver.py** use :
**./main/** as the working directory
 **./save/** as the saving directory

For a behaviour change, edit those constants  :
```python
WORKING_DIR = "main"

SAVE_DIR = "save"
```

#### NB :

OS **ctime** is used for archive management in the function **file_creation_delay** . 
This variable corresponds strictly to the creation date on Windows but can match to the last metadata change on other systems, like Linux. 
See platform documentation for more details. 

The **dirsaver_regex.py** implement an alternative filtering function, using file name and regex.
