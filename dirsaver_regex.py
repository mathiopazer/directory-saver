import os
import pathlib
import zipfile
import schedule
import functools
from datetime import datetime, timedelta



WORKING_DIR = "main"
SAVE_DIR = "save"

# ensure the directory is a pathlib object, from str or a pathlib object
# ensure this directory is usable (r,w permission)
def get_dir(directory):    

    directory_path = pathlib.Path(directory)
    try :

        os.access(directory_path, os.R_OK)
        os.access(directory_path, os.W_OK)

        if directory_path.is_dir():
            return(directory_path)

        else:
            directory.mkdir(True)
            return(directory_path)
            
    except PermissionError :
            print("Access denied to ", directory_path)
            print("Switch permission mask on this directory")

def regex_file_creation_delay (file,date) :
    creation_time = datetime.strptime(file.name.split("_")[1].split(".")[0],'%Y%m%d')
    difference = date - creation_time
    return(difference.days)

# Exception catcher for schedule : 
# Exception are catched and returned, without terminating the process
def catch_exceptions(cancel_on_failure=False):
    def catch_exceptions_decorator(job_func):
        @functools.wraps(job_func)
        def wrapper(*args, **kwargs):
            try:
                return job_func(*args, **kwargs)
            except:
                import traceback
                print(traceback.format_exc())
                if cancel_on_failure:
                    return schedule.CancelJob
        return wrapper
    return catch_exceptions_decorator


@catch_exceptions(cancel_on_failure=False)
def dirsaver() :

    today = datetime.now()
    dir_path = get_dir(WORKING_DIR)
    save_path = get_dir(SAVE_DIR)

    #### Saving the directory

    # file name convention : dir_YYYYMMDD.zip
    zip_name = dir_path.name+"_"+today.strftime("%Y%m%d")+".zip"

    save_file = save_path / zip_name

    zipf = zipfile.ZipFile(save_file,'w',zipfile.ZIP_DEFLATED)
    
    # Get every child of dir_path recursively
    for files in dir_path.glob('**/*'):
        zipf.write(files)

    #### Directory saved 

    #### Cheking old archive files

    # Use a sorted array of save_files and their time delay from now
    # To filter files to delete
    save_files_array = []

    for zipdir in save_path.glob(WORKING_DIR+'*.zip'):
        save_files_array.append(( zipdir , regex_file_creation_delay(zipdir,today) )) 

    #sorting the list in ascending age order
    save_files_array = sorted(save_files_array, key=lambda file: file[1])
    

    #iterate the list
    #i, i+1 and i-1 is in bound and prevent deletion of the oldest save
    for i in range(1,len(save_files_array)-1) :

        if ( save_files_array[i][1] > 7 # this save is more than 7 days old
        and save_files_array[i][1] - save_files_array[i-1][1] < 7  # this save is in the 7 day range
        and save_files_array[i+1][1] - save_files_array[i-1][1] <=7 ) : # Older save is also in the 7 day range
            try:
                remove_dir = save_files_array[i][0]
                remove_dir.unlink()
                # Then we can delete the file
            except (FileNotFoundError):
                print("File "+ save_files_array[i][0]+" can't be deleted :")
                print("File not found in "+ save_path)

            save_files_array[i] = save_files_array[i-1]
 

if __name__=='__main__':

    schedule.every().day.at("01:00").do(dirsaver)

    while True:
        try:
            schedule.run_pending()
        except (KeyboardInterrupt):
            print('Got SIGTERM! Terminating filesaver.py')
            break
